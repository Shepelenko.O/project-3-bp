// логика работы с локальным хранилищем
import {getRenderView, getRenderTask} from '../functions';

const isUserReg = email => {
    const users = JSON.parse(localStorage.getItem('credentuals')) || [];
    return users.some(user => email === user.email);
};

const logIn = (email, password) => {
    const users = JSON.parse(localStorage.getItem('credentuals'));
    return users.some(user => email === user.email && password === user.password);
};

const addToStorage = (obj, key )=> {
    if (JSON.parse(localStorage.getItem(key))) {
        const credentual = JSON.parse(localStorage.getItem(key)) || [];
        localStorage.setItem(key,JSON.stringify([obj, ...credentual]));
    }else localStorage.setItem(key,JSON.stringify([obj]));
};

const removeDropdownUserName = () => {
    const dropdownList = document.querySelectorAll('.dropdown.w-260.gap--bottom .dropdown__list__item');
    dropdownList.forEach(i => i.remove());
};

const getDropdownUserName = () => {
    const dropdownOpen = document.querySelector('.dropdown--open');
    const dropdownList = document.querySelectorAll('.dropdown.w-260.gap--bottom .dropdown__list__item');
    dropdownList.forEach(i => i.remove());
    const userName = JSON.parse(localStorage.getItem('credentuals')).map(element => {
        return getRenderView(element.name);
    }).join('');
    
    dropdownOpen.insertAdjacentHTML('beforeend', userName);
};

const getTaskView = () => {
    const column = document.querySelectorAll('.column.task__column');
    const beklogBoard = (JSON.parse(localStorage.getItem('tasks')) || []).map(element => {
        if(element.board === 'beklog') return getRenderTask(element.description, element.tag);
    }).join('');
    const doBoard = (JSON.parse(localStorage.getItem('tasks')) || []).map(element => {
        if(element.board === 'do') return getRenderTask(element.description, element.tag);
    }).join('');
    const doneBoard = (JSON.parse(localStorage.getItem('tasks')) || []).map(element => {
        if(element.board === 'done') return getRenderTask(element.description, element.tag);
    }).join('');
    
    column.forEach(element => {
        if (element.dataset.id === 'beklog') element.insertAdjacentHTML('beforeend', beklogBoard);
        if (element.dataset.id === 'do') element.insertAdjacentHTML('beforeend', doBoard);
        if (element.dataset.id === 'done') element.insertAdjacentHTML('beforeend', doneBoard);
    });
};

const getTaskPreView = () => {
    const column = document.querySelectorAll('.column.task__column');
    const beklogBoard = (JSON.parse(localStorage.getItem('tasks')) || []).map(element => {
        if( element.board === 'beklog') return getRenderTask(element.description, element.tag);
    }).join('');
    const doBoard = (JSON.parse(localStorage.getItem('tasks')) || []).map(element => {
        if( element.board === 'do') return getRenderTask(element.description, element.tag);
    }).join('');
    const doneBoard = (JSON.parse(localStorage.getItem('tasks')) || []).map(element => {
        if( element.board === 'done') return getRenderTask(element.description, element.tag);
    }).join('');
    
    column.forEach(element => {
        if (element.dataset.id === 'beklog') element.insertAdjacentHTML('beforeend', beklogBoard);
        if (element.dataset.id === 'do') element.insertAdjacentHTML('beforeend', doBoard);
        if (element.dataset.id === 'done') element.insertAdjacentHTML('beforeend', doneBoard);
    });
};

export {isUserReg, logIn, addToStorage, getDropdownUserName ,removeDropdownUserName, getTaskView, getTaskPreView};