import {addToStorage, getDropdownUserName, removeDropdownUserName, getTaskView} from '../api';

const taskColumn = document.querySelectorAll('.column.task__column');
const formTask = document.querySelector('.form.form--task');
const dropdownExecutor = document.querySelector('.dropdown.w-260.gap--bottom .dropdown__toggle');
const dropdownW_260Gap_bottom = document.querySelector('.dropdown.w-260.gap--bottom');
const inputExecutor = document.getElementById('executor');
const dropdownTag = document.querySelector('.dropdown.tag__dropdown .dropdown__toggle');
const dropdownTagDropdown = document.querySelector('.dropdown.tag__dropdown');
const inputTag = document.getElementById('tag');

const newTask = {};

// email validation
const emailValidation = email => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};
// password validation
const passwordValidation = pass => {
    const re = /(?=.{8,})/;
    return re.test(pass);
};
// user fullname validation
const nameValidation = name => {
    const re = /^([a-zA-Z]{2,}\s[a-zA-Z]{1,}'?-?[a-zA-Z]{2,}\s?([a-zA-Z]{1,})?)/;
    return re.test(name);
};
// function move
const move = (showBlock, hiddenBlock) => {
    showBlock.style.display= 'block';
    hiddenBlock.style.display = 'none';
};
// Error
const errorFunc = elem => {
    return elem.style.border = '1px solid red';
};
// render users name in new tack
const getRenderView = name => {
    return `
    <div class="dropdown__list__item">
        <p class="form__input">${name}</p>
    </div>
    `;
};
// dropdown function executor select
const getDropdownElement = (elem1, elem2, nameClass1, nameClass2, bool1, bool2) => {
    elem1.classList.toggle(nameClass1, bool1);
    elem2.classList.toggle(nameClass2, bool2);
};

const getRenderTask = (description, tag) => {
    return `
    <a href="#" class="task task__block">
        <p class="task__description">${description}</p>
        <div class="flex">
            <img src="img/userpic.jpg" class="task__executor" alt="">
            <p class="task__label development">${tag}</p>
        </div>
    </a>
    `;
};

const hiddenAddTackBlock = () => {
    taskColumn.forEach(elem => elem.style.display = 'none');
    formTask.style.display = 'block';
};

const dropdownInputExecutor = () => {
    dropdownExecutor.addEventListener('click', () => {
        if (dropdownExecutor.classList.contains('active')) {
            getDropdownElement(dropdownExecutor, dropdownW_260Gap_bottom, 'active', 'dropdown--open', false, false);
            removeDropdownUserName();
        } else {
            getDropdownElement(dropdownExecutor, dropdownW_260Gap_bottom, 'active', 'dropdown--open', true, true);
            getDropdownUserName();
            const dropdownListItemUser = dropdownW_260Gap_bottom.querySelectorAll('.dropdown__list__item');
            dropdownListItemUser.forEach(element => element.addEventListener('click', event => {
                inputExecutor.value = event.target.textContent.trim();
                getDropdownElement(dropdownExecutor, dropdownW_260Gap_bottom, 'active', 'dropdown--open', false, false);
                removeDropdownUserName();
                newTask.executor = inputExecutor.value;
            }));
        };
    });
};

const dropdownInputTag = () => {
    dropdownTag.addEventListener('click', () => {
        if (dropdownTag.classList.contains('active')) {
            getDropdownElement(dropdownTag, dropdownTagDropdown, 'active', 'dropdown--open', false, false);
        } else {
            getDropdownElement(dropdownTag, dropdownTagDropdown, 'active', 'dropdown--open', true, true);
            const dropdownListItemTag = dropdownTagDropdown.querySelectorAll('.dropdown__list__item');
            dropdownListItemTag.forEach(element => element.addEventListener('click', event => {
                const labelTagName = dropdownTag.querySelector('.label');
                labelTagName.textContent = event.target.textContent.trim();
                getDropdownElement(dropdownTag, dropdownTagDropdown, 'active', 'dropdown--open', false, false);
                newTask.tag = event.target.textContent.trim();
            }));
        };
    });
};

const taskSubmit = () => {
    formTask.querySelector('input[type="submit"]').addEventListener('click', event => {
        event.preventDefault();
        const titleNewTack =  formTask.querySelector('input[id="taskTitle"]').value;
        const deadline = document.getElementById('deadline');
        const taskDescription = document.getElementById('taskDescription');
        newTask.description = taskDescription.value;
        newTask.title = titleNewTack;
        newTask.deadline = deadline.value;
        addToStorage(newTask, 'tasks');
        getTaskView();
        taskDescription.value = '';
        deadline.value = '';
        formTask.querySelector('input[id="taskTitle"]').value = '';
        inputExecutor.value = '';
        inputTag.value = '';
        taskColumn.forEach(elem => elem.style.display = 'block');
        formTask.style.display = 'none';
    });
};

const addTask = () => {
    hiddenAddTackBlock();
    dropdownInputExecutor();
    dropdownInputTag();
    taskSubmit();
};

const navigationsMove = () => {
    const tabNavigation = document.querySelector('.tab__navigation');
    const links = tabNavigation.querySelectorAll('li');
    links.forEach(link => {
        link.addEventListener('click', () => {
            if (link.dataset.id === 'task-link') window.location.href = 'task-board.html';
            if (link.dataset.id === 'kanban-link') window.location.href = 'task-description.html';
            if (link.dataset.id === 'calendar-link') window.location.href = 'calendar.html';
        });
    });
};

export {emailValidation,passwordValidation, nameValidation, move, errorFunc, getRenderView, getDropdownElement, getRenderTask, addTask, navigationsMove, newTask};