import '../../../css/style.scss';
import {emailValidation,passwordValidation, nameValidation, move, errorFunc} from '../../../functions';
import {isUserReg, logIn, addToStorage} from '../../../api';

const log = document.getElementById('log');
const reg = document.getElementById('reg');
const formLogin = document.querySelector('.form--login');
const formRegistration = document.querySelector('.form--registration');
const formSetup = document.querySelector('.form--setup');
const emailReg = document.querySelector(`[data-id="email-reg"]`);
const emailLogin = document.querySelector(`[data-id="email-login"]`);
const passReg = document.querySelector(`[data-id="pass-reg"]`);
const passLogin = document.querySelector(`[data-id="pass-login"]`);
const user = document.getElementById('user');
const radioBtn = document.querySelectorAll('.radiobtn__immitation');

const userObj = {};

// move to login block
log.addEventListener('click', () => move(formLogin,formRegistration));
// move to registration block
reg.addEventListener('click', () => move(formRegistration,formLogin));
// Registration step 1
formRegistration.querySelector('input[type="submit"]').addEventListener('click', event => {
    event.preventDefault();
    const emailRegValue = emailReg.value;
    const passRegValue = passReg.value;
    const userValue = user.value;
    if (emailValidation(emailRegValue) && passwordValidation(passRegValue) && nameValidation(userValue)){
        if (isUserReg(emailRegValue)) {
            alert(`Пользователь с email ${emailRegValue} уже зарегистрирован`);
        }else {
            userObj.email = emailRegValue;
            userObj.password = passRegValue;
            userObj.name = userValue;
            move(formSetup,formRegistration);
        };
    };
    if (!emailValidation(emailRegValue)) errorFunc(emailReg);
    if (!passwordValidation(passRegValue)) errorFunc(passReg);
    if (!nameValidation(userValue)) errorFunc(user);
});
// Registration step 2 (job)
formSetup.querySelector('input[type="submit"]').addEventListener('click', event => {
    event.preventDefault();
    const checkedBtn = [...radioBtn].find(elem => elem.querySelector('input[type="radio"]').checked);
    userObj.job = checkedBtn.querySelector('input').id;
    addToStorage(userObj, 'credentuals');
    window.location.href = 'task-board.html';
});
// Login
formLogin.querySelector('input[type="submit"]').addEventListener('click', event => {
    event.preventDefault();
    const emailLoginValue = emailLogin.value;
    const passLoginValue = passLogin.value;
    if (logIn(emailLoginValue, passLoginValue)) window.location.href = 'task-board.html';
    else alert(`Пользователя не найдено`);
});
