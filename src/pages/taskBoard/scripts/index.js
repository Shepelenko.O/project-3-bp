import '../../../css/style.scss';
import {getTaskPreView} from '../../../api';
import {addTask, navigationsMove, newTask} from '../../../functions';

navigationsMove();

getTaskPreView();
// localStorage.removeItem('tasks');
const beklogTackPlus = document.getElementById('beklog-tack-plus');
const doTaskPlus = document.getElementById('do-tack-plus');
const doneTaskPlus = document.getElementById('done-tack-plus');
const column = document.querySelectorAll('.column.task__column');

beklogTackPlus.addEventListener('click', () => {
    newTask.board = 'beklog';
    addTask();
});
doTaskPlus.addEventListener('click', () => {
    newTask.board = 'do';
    addTask();
});
doneTaskPlus.addEventListener('click', () => {
    newTask.board = 'done';
    addTask();
});